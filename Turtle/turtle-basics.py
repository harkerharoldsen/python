import turtle
import random
# main function
def main():
	screen = turtle.Screen()
	screen.screensize(1000, 1000)
	t1 = turtle.Turtle()

	t2 = turtle.Turtle()
	t = turtle.Turtle()
	#t.tracer(0, 0)
	t.ht()
	t1.ht()
	t2.ht()
	t.speed(0)
	t.penup()
	t.width(10)
	t.goto(250,250)
	t.pendown()
	t.fd(1)
	t.lt(-90)
	t.penup()
	t.fd(50)
	t.pendown()
	t.lt(45)
	
	t.circle(90,100)
	t.lt(35)
	t.penup()
	t.fd(50)
	t.pendown()
	t.fd(1)
	t.penup() 
	t.goto(-250,250)
	t.pendown()
	t.fd(1)
	t.lt(180)
	t.penup()
	t.fd(50)
	t.pendown()
	t.lt(45)
	
	t.circle(-90,-100)
	t.lt(35)
	t.penup()
	t.fd(50)
	t.pendown()
	t.fd(1)
	t.penup() 
	t.goto(-250,-250)
	t.pendown()
	t.fd(1)
	t.lt(150)
	t.penup()
	t.fd(50)
	t.pendown()
	t.lt(-75)
	
	t.fd(100)
	t.lt(-65)
	t.penup()
	t.fd(50)
	t.pendown()
	t.fd(1)
	t.penup() 
	t.goto(-250,-250)
	# fill poly start
	t.goto(0,0)
	t.pendown()
	a = 0
	a1 = 0
	a2 = 0
	b1 = 0
	b2 = 0
	b = 0
	t1.penup()
	t1.goto(-250,-250)
	t1.pendown()
	t2.penup()
	t2.goto(250,250)
	t2.pendown()
	while(a !=100000):
		R = random.random()
		B = random.random()
		G = random.random()

		t.color(R, G, B)
		t.width(1)
		t.circle(a,b)
		a = a + 1
		b = b + 1
		R1 = random.random()
		B1 = random.random()
		G1 = random.random()

		t1.color(R1, G1, B1)
		t1.width(1)
		t1.circle(a1,b1)
		a1 = a1 + 1
		b1 = b1 + 1
		R2 = random.random()
		B2 = random.random()
		G2 = random.random()

		t2.color(R2, G2, B2)
		t2.width(1)
		t2.circle(a2,b2)
		a2 = a2 + 1
		b2 = b2 + 1
	screen.exitonclick()	
	

if __name__ == "__main__":
		main()
