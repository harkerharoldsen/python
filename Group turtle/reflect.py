import turtle
import random
def mirror():
	turtle.TurtleScreen._RUNNING = True
	screen = turtle.Screen()
	a = []
	t = turtle.Turtle()
	t.penup()

	t.goto(-20,0)
	turtle.penup()

	turtle.goto(20,0)
	t.pendown()
	turtle.pendown()
	t.setheading(180)
	t.speed(0)
	turtle.speed(0)
	def fxn():
		gate = 0
		turtle.forward(20)
		a.append(turtle.pos())
		t.forward(20)


	def fxn1():
		turtle.right(90)
		t.left(90)
	def fxn2():
		turtle.left(90)
		t.right(90)
		
	def fxn3():
		gate = 0
		turtle.fd(-20)
		t.fd(-20)
		a.append(turtle.pos())
	def fxn4():
		r = lambda: random.randint(0,255)
		d = ('#%02X%02X%02X' % (r(),r(),r()))
		ra = lambda: random.randint(0,255)
		da = ('#%02X%02X%02X' % (ra(),ra(),ra()))
		turtle.pencolor(d)
		t.pencolor(da)
	def fxn5():
		turtle.circle(90,360)
		t.circle(-90,360)
	def fxn6():
		turtle.forward(50)
		turtle.right(90)
		turtle.forward(50)
		turtle.right(90)
		turtle.forward(50)
		turtle.right(90)
		turtle.forward(50)
		turtle.right(90)
		t.forward(50)
		t.right(-90)
		t.forward(50)
		t.right(-90)
		t.forward(50)
		t.right(-90)
		t.forward(50)
		t.right(-90)
	def fxn7():
		t.right(180)
		for i in range(0,90):
			turtle.circle(i,270)
			t.circle(-i,270)
	def fxn8():
		turtle.right(120)
		turtle.fd(90)
		turtle.right(120)
		turtle.fd(90)
		turtle.right(120)
		turtle.fd(90)
		t.right(-120)
		t.fd(90)
		t.right(-120)
		t.fd(90)
		t.right(-120)
		t.fd(90)
	def fxn9():
		t.fd(90)
	turtle.listen()
	turtle.onkey(fxn,'Up')
	turtle.onkey(fxn1,'Right')
	turtle.onkey(fxn2,'Left')
	turtle.onkey(fxn3,'Down')
	turtle.onkey(fxn4,'r')
	turtle.onkey(fxn5,'t')
	turtle.onkey(fxn6,'y')
	turtle.onkey(fxn7,'u')
	turtle.onkey(fxn8,'i')
	try:
		t.penup()
		t.goto(-20,0)
		turtle.penup()
		turtle.goto(20,0)
		t.pendown()
		turtle.pendown()
		t.setheading(180)
		t.speed(0)
		turtle.speed(0)
	finally:
		turtle.Terminator()
if __name__ == "__reflect__":
		reflect()

