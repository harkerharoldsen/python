import turtle
import time

def poly(t,x,y,size,side,color):
	try:
		turtle.TurtleScreen._RUNNING = True
		t.pencolor('#000000')
		t.fillcolor(color)
		t.begin_fill()
		for n in range (0,4):
			t.forward(size)
			t.left(360/side)
		t.end_fill()

		turtle.update()
	finally:
		turtle.Terminator()
def tree(t,x,y,):
	try:
		turtle.TurtleScreen._RUNNING = True
		# color list
		h = [-50 , -50 , -100 ,  -70 , -120 ,-70, -100, -10 , 80 , 50,   100 ,  50,   100 ,  50 , 50,  0 ]
		k = [-300 ,-200 ,-200 , -100 , -100 , 0 ,   0,   100 , 0 ,    0,  -100 , -100, -200 , -200, -300, -300 ]
		# design design list
		t.penup()
		t.goto(x,y)
		t.pendown()

		print(t,x,y)
		t.width(3)
		
		t.color("#00ff00")
		t.begin_fill()
		for n  in range(0,16):
			t.goto(h[n],k[n])
			print(h[n],k[n])
		t.end_fill()
	finally:
		turtle.Terminator()
def xmastree():
	try:
		turtle.TurtleScreen._RUNNING = True
		w = turtle.Screen()
		# setup the screen size
		w.setup(440,440)
	  
	# set the background color
		w.bgcolor("#000011")
		x = -200; y = 0
		w.clear()
		w.bgcolor("#ffffff")
		t = turtle.Turtle()
		circle = turtle.Turtle()
		circle.shape('circle')
		circle.color ('red')
		circle.speed('fastest')
		circle.up()
		
		

		#turtle.tracer(0, 0)

		   
		
		tree(t,0,-300)
		circle.goto (-9,110)
		circle.stamp()     
		circle.goto (30,50)       
		circle.stamp() 
		circle.goto (-9,50)       
		circle.stamp() 
		circle.goto (-50,50)       
		circle.stamp() 
		circle.goto (-95,5)       
		circle.stamp() 
		circle.goto (80,-2)       
		circle.stamp() 
		circle.goto (100,-100)       
		circle.stamp() 
		circle.goto (-115,-100)       
		circle.stamp() 
		w.exitonclick()
	finally:
		turtle.Terminator()
if __name__ == '__main__':
	xmastree()

'''
cl = ["#f8f8f8","#e8e8e8" ,"#d8d8d8" ,"#b8b8b8" ,
				"#585858" ,"#383838" ,"#282828" , "#181818" ,
				"#ab4642" ,	"#dc9656" ,"#f79a0e" ,"#538947" ,
				"#4b8093" ,"#7cafc2" ,"#96609e" ,"#a16946" ]

'''
