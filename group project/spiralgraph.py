import turtle
import random

color = ['#959DCB','#50507A','#32374D']


def spiral():
	try:
		turtle.TurtleScreen._RUNNING = True
		screen = turtle.Screen()
		t = turtle.Turtle()
		screen.bgcolor('#32374D')
		t.speed(0)
		for i in range(500):
			t.pencolor(random.choice(color))
			t.forward(5+i)
			t.right(70)
	
	finally:
		turtle.Terminator()
	
def spiralgraph():
	try:
		turtle.TurtleScreen._RUNNING = True
		screen = turtle.Screen()
		t = turtle.Turtle()
		#t.tracer(0, 0)
		t.speed(0)
		for x in range(15):
			t.goto(0,0)
			t.width(2)
			t.pendown
			t.pencolor("#F07178")
			t.circle(125)
			t.lt(5)
			t.pencolor("#F78C6C")
			t.circle(125)
			t.lt(5)
			t.pencolor("#FFCB6B")
			t.circle(125)
			t.lt(5)
			t.pencolor("#C3E88D")
			t.circle(125)
			t.lt(5)
			t.pencolor("#89DDFF")
			t.circle(125)
			t.lt(5)
			t.pencolor("#82AAFF")
			t.circle(125)
			t.lt(5)
			t.pencolor("#C792EA")
			t.circle(125)
			t.lt(5)
			t.pencolor("#FF5370")
			t.circle(125)
			t.lt(5)
		

	finally:
		turtle.Terminator()
def main_spiral():
	try:
		spiral()
		spiralgraph()
	finally:
		turtle.Terminator()
if __name__ == "__main__":
		main_spiral()
		
		
		
