import turtle 
def star():
	try:
		turtle.TurtleScreen._RUNNING = True
		screen = turtle.Screen()
		t = turtle.Turtle()
		t.penup()
		t.hideturtle()
		t.speed(0)
		angle=170
		side=50
		pointies = 20
		ROTATION = 360/pointies

		angle_left=angle
		angle_right=angle
		t.goto(-250,250)
		t.pendown()

		t.color("red")
		for p in range(pointies):
			t.forward(side)
			t.right(angle_right)
			t.forward(side)
			t.left(angle_left)
			t.right(ROTATION)
			t.end_fill()
	finally:
		turtle.Terminator()
def star2():
	try:
		screen = turtle.Screen()
		t = turtle.Turtle()
		turtle.TurtleScreen._RUNNING = True
		t.hideturtle()
		t.penup()
		t.speed(0)
		angle=170
		side=60
		pointies = 20
		ROTATION = 360/pointies

		angle_left=angle
		angle_right=angle
		t.goto(250,-250)
		t.pendown()

		t.color("yellow")
		for p in range(pointies):
			t.forward(side)
			t.right(angle_right)
			t.forward(side)
			t.left(angle_left)
			t.right(ROTATION)
			t.end_fill()
	finally:
		turtle.Terminator()
def star3():
	try:
		screen = turtle.Screen()
		t = turtle.Turtle()
		turtle.TurtleScreen._RUNNING = True
		t.penup()
		t.hideturtle()
		t.speed(0)
		angle=170
		side=55
		pointies = 20
		ROTATION = 360/pointies

		angle_left=angle
		angle_right=angle
		t.goto(300,300)
		t.pendown()

		t.color("blue")
		for p in range(pointies):
			t.forward(side)
			t.right(angle_right)
			t.forward(side)
			t.left(angle_left)
			t.right(ROTATION)
			t.end_fill()
	finally:
		turtle.Terminator()
def star4():
	try:
		screen = turtle.Screen()
		t = turtle.Turtle()
		turtle.TurtleScreen._RUNNING = True
		t.penup()
		t.hideturtle()
		t.speed(0)
		angle=170
		side=45
		pointies = 20
		ROTATION = 360/pointies

		angle_left=angle
		angle_right=angle
		t.goto(-250,-250)
		t.pendown()

		t.color("green")
		for p in range(pointies):
			t.forward(side)
			t.right(angle_right)
			t.forward(side)
			t.left(angle_left)
			t.right(ROTATION)
			t.end_fill()
	finally:
		turtle.Terminator()			
def thespiral():	
	try:
		screen = turtle.Screen()
		screen.bgcolor('#000000')
		t = turtle.Turtle()
		t.hideturtle()
		t.speed(0)
		for i in range(207):
			t.width(5)
			t.pencolor("#ff00b3")#color
			t.forward(1+i)
			t.right(50)
			

		screen.exitonclick()
	finally:
		turtle.Terminator()		
def pinkspiralmain():
	try:
		
		star()
		star2()
		star3()
		star4()
		thespiral()
		screen.exitonclick()
	finally:
		turtle.Terminator()	
if __name__ == "__main__":
	pinkspiralmain()

