from mcpi.minecraft import Minecraft
from random import randint
from mcpi import block
from time import sleep
def init():
    #ip = "192.168.7.84"
    ip = "127.0.0.1"
    mc = Minecraft.create(ip, 4711)
    #mc.setting("world_immutable",True)
    x, y, z = mc.player.getPos()
    return mc
def box(mc):
	x, y, z = mc.player.getPos()
	mc.setBlocks(x-10,y-5,z-10,x+10,y-5,z+10, 5)
	mc.setBlocks(x-10,y-5,z-10,x+10,y+5,z-10, 5)
	mc.setBlocks(x-10,y-5,z+10,x+10,y+5,z+10, 5)
	mc.setBlocks(x+10,y-5,z-10,x+10,y+5,z+10, 5)
	mc.setBlocks(x-10,y-5,z-10,x-10,y+5,z+10, 5)
	mc.setBlocks(x-10,y+5,z-10,x+10,y+5,z+10, 5)
	mc.setBlock(x-10,y-4 ,z-0,64)
	mc.setBlock(x-10,y-3 ,z-0,64)
def roof(mc):
	x, y, z = mc.player.getPos()
	mc.setBlocks(x-10,y+5,z-10,x+10,y+5,z+10, 53,2)
	mc.setBlocks(x+10,y+5,z-10,x+10,y+5,z+10, 53,2)
	mc.setBlocks(x-10,y+5,z-9,x+10,y+6,z+9, 5)
	mc.setBlocks(x+10,y+6,z-9,x+10,y+6,z+9, 53,4)
	mc.setBlocks(x+10,y+6,z-9,x+10,y+6,z+9, 53,2)
	mc.setBlocks(x-10,y+6,z-8,x+10,y+6,z+8, 5)
	mc.setBlocks(x-10,y+7,z-8,x+10,y+7,z+8, 53,2)
	mc.setBlocks(x-10,y+7,z-7,x+10,y+7,z+7, 5)
	mc.setBlocks(x-10,y+8,z-7,x+10,y+8,z+7, 53,2)
	mc.setBlocks(x-10,y+8,z-6,x+10,y+8,z+6, 5)
	mc.setBlocks(x-10,y+9,z-6,x+10,y+9,z+6, 53,2)
	mc.setBlocks(x-10,y+9,z-5,x+10,y+9,z+5, 5)
	mc.setBlocks(x-10,y+10,z-5,x+10,y+10,z+5, 53,2)
	mc.setBlocks(x-10,y+10,z-4,x+10,y+10,z+4, 5)
	mc.setBlocks(x-10,y+11,z-4,x+10,y+11,z+4, 53,2)
	mc.setBlocks(x-10,y+11,z-3,x+10,y+11,z+3, 5)
	mc.setBlocks(x-10,y+12,z-3,x+10,y+12,z+3, 53,2)
	mc.setBlocks(x-10,y+12,z-2,x+10,y+12,z+2, 5)
	mc.setBlocks(x-10,y+13,z-2,x+10,y+13,z+2, 53,2)
	mc.setBlocks(x-10,y+13,z-1,x+10,y+13,z+1, 5)
	mc.setBlocks(x-10,y+14,z-1,x+10,y+14,z+1, 53,2)
	mc.setBlocks(x-10,y+14,z-0,x+10,y+14,z+0, 5)
	

	
def main():
	mc = init()
	box(mc)
	roof(mc)																																																													
	
	 
if __name__ == "__main__":
	main()





